##########
安装与使用
##########

软电话终端类型库是一个 `Microsoft .NET <http://www.microsoft.com/net>`_ 类型库，由 `C++/CLI <http://wikipedia.org/wiki/C%2B%2B/CLI>`_ 语言编写。所以，应用程序开发者必须使用.NET程序对它进行调用。

文件 ``Hesong.Sipua.dll`` 包含了软电话的全部代码实现与接口定义。
开发者需要在自己的项目中引用该类型库，并将它发布到目标环境。

发布目录说明：

=========== ================
发布目录    说明
=========== ================
net/2.0/    用于 .NET 2.0 的程序包
net/4.0/    用于 .NET 4.0 Full Pack 的程序包
netcp/4.0/  用于 .NET 4.0 Client Pack 的程序包
=========== ================

*********
系统要求
*********

==============
支持的操作系统
==============
* Windows XP SP3
* Windows Server 2003 SP2
* Windows Vista SP1 或更高版本
* Windows Server 2008
* Windows 7
* Windows Server 2008 R2
* Windows 7 SP1
* Windows Server 2008 R2 SP1

===============
支持的体系结构
===============
* x86
* x64
    需要Windows 64bit在32bit兼容模式下使用

====================
支持的.Net Framework
====================

* MMicrosoft .NET Framework 2.0 Service Pack 2

    .NET Framework 运行时和关联文件（这些文件是运行大多数客户端应用程序所必需的）。

    `下载页面 <http://www.microsoft.com/zh-cn/download/details.aspx?id=1639>`_

* Microsoft .NET Framework 4 Client Profile

    .NET Framework 运行时和关联文件（这些文件是运行大多数客户端应用程序所必需的）。

    `下载页面 <http://www.microsoft.com/zh-CN/download/details.aspx?id=24872>`_

* Microsoft .NET Framework 4

    .NET Framework 运行时和关联文件（这些文件是运行和开发面向 .NET Framework 4 的应用程序所必需的）。

    `下载页面 <http://www.microsoft.com/zh-cn/download/details.aspx?id=17718>`_

================================
支持的Visual C++ Redistributable
================================

* Microsoft Visual C++ 2005 Redistributable Package (x86)

    使用 Visual C++ 开发的应用程序所需的 Visual C++ 库的运行时组件。

    .. attention:: 该组件 **必须安装** ，即使使用 .NET 4.0 程序包，也需要该组件。

* Visual C++ Redistributable for Visual Studio 2012 Update 4
    
    Visual Studio 2012 生成的 C++ 应用程序所必需的运行时组件。

    `下载页面 <http://www.microsoft.com/zh-CN/download/details.aspx?id=30679>`_

    .. note:: 如果使用 .NET 4.0 程序包，该组件必须安装。

==================
依赖的第三方类型库
==================

该类型库依赖的唯一第三方类型库是 `Apache log4net™ <http://logging.apache.org/log4net/>`_ 。

发布目录下同一个子目录中的 ``log4net.dll`` 与 ``Hesong.Sipua.dll`` 对于.NET的版本的要求一致，它们需要一同使用。

目前使用的log4net版本是 ``1.2.13`` 。

========
硬件要求
========
* 建议的最低要求：Pentium 1 GHz 或更快，512 MB RAM 或更大
* 最小磁盘空间：10 MB
* 全双工声卡
* 麦克风
* 头戴式耳机

*************************
在 Vistual Studio 中使用
*************************

* Visual Studio 2005

    测试通过

* Visual Studio 2008

    未测试

* Visual Studio 2010

    未测试

* Visual Studio 2012

    测试通过

* Visual Studio 2013

    未测试