*****
介绍
*****

和声 `SIP <http://wikipedia.org/wiki/Session_Initiation_Protocol>`_ 软电话终端 SDK 是用于 `Microsoft Windows <http://windows.microsoft.com>`_ 桌面环境的 SIP 客户端开发包，它主要包括：

    * 软电话终端类型库
    * 相关文档
    * 例子代码
    * 授权使用协议

软电话终端类型库是该SDK的核心部分，开发者通过调用该类型库，可实现常见的SIP终端音频通信功能。