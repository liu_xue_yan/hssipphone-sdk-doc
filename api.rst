########
API 参考
########

由于本类库使用  `C++/CLI <http://wikipedia.org/wiki/C%2B%2B/CLI>`_ 语言实现，所以 API 参考中的接口定义采用了 C++ 语言的书写风格。但是，由于 C++/CLI 与 标准 C++ 的差异性，接口描述可能与实际情况略有不同，请开发者以实际情况为准。

.. toctree::
   :maxdepth: 2

    Hesong::Sipua::Lib <Hesong.Sipua.Lib>

    Hesong::Sipua::Account <Hesong.Sipua.Account>

    Hesong::Sipua::CallInfo <Hesong.Sipua.CallInfo>

    Hesong::Sipua::RegInfo <Hesong.Sipua.RegInfo>
