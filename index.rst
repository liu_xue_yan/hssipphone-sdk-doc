.. HSSIPUA 开发手册 documentation master file, created by
   sphinx-quickstart on Wed Jul 16 16:43:50 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

########################
和声SIP软电话终端SDK手册
########################

.. include:: intro.rst

*****
目录
*****

.. toctree::
   :maxdepth: 2

   介绍 <intro>

   安装与使用 <install>

   API 参考 <api>


**********
索引与搜索
**********

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
