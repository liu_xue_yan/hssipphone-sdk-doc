Hesong::Sipua::RegInfo
########################

.. cpp:namespace:: Hesong::Sipua

.. cpp:class:: RegInfo

    注册信息

构造
****

.. cpp:function:: RegInfo::RegInfo(void* info)

    :param info: 注册信息指针

    开发者请 **不要** 实例化该类

析构
****

.. cpp:function:: ~RegInfo::RegInfo(void* info)

    :param info: 注册信息指针

    开发者请 **不要** 实例化该类

属性
****

.. cpp:member:: int RegInfo::Code

    注册状态编码

.. cpp:member:: String* RegInfo::Reason

    注册状态描述

.. cpp:member:: int RegInfo::Expiration

    注册过期时间（秒）
