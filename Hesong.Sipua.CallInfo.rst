Hesong::Sipua::CallInfo
########################

.. cpp:namespace:: Hesong::Sipua

.. cpp:class:: CallInfo

    呼叫信息

    这个类用于访问与控制呼叫、通话

构造
****

该类的实例将在发起外呼或者收到来电时被自动新建，开发者 **不要** 手动实例化该类。

.. cpp:function:: CallInfo::CallInfo()

.. cpp:function:: CallInfo::CallInfo(void* info)

析构
****

.. cpp:function:: ~CallInfo::CallInfo()

方法
****

.. cpp:function:: void CallInfo::StartRecord()

    开始录音

    调用该方法后，如果已经设置对象的 :cpp:member:`RecFile` 属性为有效的文件名，那么通话录音会被记录到该文件。录音格式是 256kB/s 的 PCM。

.. cpp:function:: void CallInfo::StopRecord()

    停止录音

    .. note:: 在开始录音之后，开发者不必调用该方法停止录音，因为本类库会在通话结束时自动结束已经开始的录音。

.. cpp:function:: void CallInfo::Answer()

    应答

    在事件 :cpp:func:`Account::OnIncomingCall` 中，调用该方法可以接通来电

.. cpp:function:: void CallInfo::Hangup()

    挂断

    * 在事件 :cpp:func:`Account::OnIncomingCall` 中，调用该方法可以拒接来电
    * 在通话中调用该方法，可结束通话

.. cpp:function:: void CallInfo::SendDtmf(String* digits)

    :param digits: DTFM 码字符串

    发送DTMF码

属性
****

.. cpp:member:: float CallInfo::InputVolume

    输入音量

    输入音量是指：在本机上播放的，来自对端的声音的音量。

    其值越大，音量越大。 ``0`` 表示静音。

    :默认值: ``2.0``

    .. note:: 调整本属性不会影响 Windows 操作系统的音量设置。如果 Windows 播放音量很小，那么，即使本属性值很大，最终听到的声音也会很小。同理，如果 Windows 播放被静音，即使本属性值很大，也会无声。

.. cpp:member:: float CallInfo::OutputVolume

    输出音量

    输出音量是指：在对端播放的，来自本机的声音的音量。

    其值越大，音量越大。 ``0`` 表示静音。

    :默认值: ``10.0``

    .. note:: 调整本属性不会影响 Windows 操作系统的音量设置。如果 Windows 采集音量很小，那么，即使本属性值很大，最终听到的声音也会很小。同理，如果 Windows 采集被静音，即使本属性值很大，也会无声。

.. cpp:member:: int CallInfo::State

    呼叫状态编码

.. cpp:member:: String* CallInfo::StateText

    呼叫状态描述文本

.. cpp:member:: int CallInfo::LastStatus

    上次收到的状态码，可以用作 cause code

.. cpp:member:: String* CallInfo::LastStatusText

    上次收到的状态码描述文本

.. cpp:member:: String* CallInfo::RemoteInfo

    对端信息

.. cpp:member:: String* CallInfo::RemoteInfoContact

    对端被叫/主叫/联系人信息

.. cpp:member:: Boolean CallInfo::IsActive

    通话是否处于活动状态

.. cpp:member:: String* CallInfo::RecFile

    录音文件

    只有将本属性设置为一个有效的文件名后，调用 :cpp:func:`CallInfo::StartRecord` 方可将媒体流写入到录音文件。

