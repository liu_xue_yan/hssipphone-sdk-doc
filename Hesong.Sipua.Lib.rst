Hesong::Sipua::Lib
###################

.. cpp:namespace:: Hesong::Sipua

.. cpp:class:: Lib

    该类是SIP软电话终端的核心。在进行任何其它操作时前，都必须实例化该类。

构造
****

.. cpp:function:: Lib::Lib()

    使用默认参数的构造函数。使用该构造函数时，SIP端口为5060，RTP启示端口为4000

.. cpp:function:: Lib::Lib(unsigned int sip_udp_port, unsigned int rtp_udp_port)

    :param sip_udp_port: 该 SIP UA 所使用的 SIP UDP 端口。0表示自动寻找一个未被占用的端口。建议使用 SIP 协议的默认端口 ``5060``
    :param rtp_udp_port: 该 SIP UA 所使用的 RTP UDP 端口。0表示自动寻找一个未被占用的端口。建议使用 ``1024`` 以上的端口，如 ``4000``。注意 RTCP 会自动占用该RTP端口数值 +1 的端口。

.. attention::

    在构造该类的实例时，SIP协议栈所需的全局数据被初始化。所以在进行任何其它操作之前，必须首先新建一个 :cpp:class:`Lib` 实例，且 **不可新建多个实例** 。

析构
****

.. cpp:function:: Lib::~Lib()

    在析构时，所有的全局资源都会被释放。.NET 程序一般不应主动调用析构函数。

属性
****

.. cpp:member:: static Lib* Lib::Instance

    在实例化后，通过该静态属性获取唯一实例

.. cpp:member:: Lib::AccountList* Lib::Accounts

    SIP账户列表

类型
****

AccountList
============

.. cpp:class:: Lib::AccountList: public System::Collections::Generic::IList<Account>

    账户列表类

    它是 :cpp:class:`Lib` 类的一个嵌套子类。该类型实现了 .NET 系统框架的 `System::Collections::Generic::IList <http://msdn.microsoft.com/en-us/library/5y536ey6%28v=vs.110%29.aspx>`_ 接口。
    这个 ``List`` 容器类的元素是 :cpp:class:`Account` 对象。开发者通过增减容器中的元素，来增加或者删除SIP账户。

方法
----

.. cpp:function:: void Lib::AccountList::Add(Account* item)

    :param item: 账户对象

    添加账户

.. cpp:function:: void Lib::AccountList::Clear()

    清空账户

.. cpp:function:: bool Lib::AccountList::Contains(Account* item)

    :param item: 账户
    :return: 是否存在

    是否存在账户

.. cpp:function:: void Lib::AccountList::CopyTo(array<Account*>* array, int arrayIndex)

.. cpp:function:: int Lib::AccountList::IndexOf(Account* item)

.. cpp:function:: void Lib::AccountList::Insert(int index, Account* item)

.. cpp:function:: bool Lib::AccountList::Remove(Account* item)

.. cpp:function:: void Lib::AccountList::RemoveAt(int index)

属性
----

.. cpp:member:: int Lib::AccountList::Count

    获取账户列表中，账户的数量

