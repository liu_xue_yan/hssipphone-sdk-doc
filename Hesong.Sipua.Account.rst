Hesong::Sipua::Account
########################

.. cpp:namespace:: Hesong::Sipua

.. cpp:class:: Account

    SIP账户

构造
****

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, String* password, unsigned int regTimeout, unsigned int regRetry, unsigned int regFirstRetry, String* label)

    :param registerServer: 注册服务器地址。地址格式为： ``host[:port]`` 。如果不提供端口，默认使用5060端口。
    :param project: 项目名称
    :param userName: 注册用户名
    :param password: 注册密码
    :param regTimeout: 注册超时值（秒）
    :param regRetry: 注册重试时间（秒）
    :param regFirstRetry: 首次注册重试时间（秒）
    :param label: 账户标签

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, String* password, unsigned int regTimeout, unsigned int regRetry, unsigned int regFirstRetry)

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, unsigned int regTimeout, unsigned int retryInterval, unsigned int firstRetryInterval, String* label)

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, unsigned int regTimeout, unsigned int retryInterval, unsigned int firstRetryInterval)

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, String* password, String* label)

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName, String* label)

.. cpp:function:: Account::Account(String* registerServer, String* project, String* userName)

析构
*****

.. cpp:function:: ~Account::Account()

方法
****

.. cpp:function:: int Account::Add()

    将账户数据添加到全局数据。如果不调用该方法，就无法针对账户实例做出任何呼叫、接听、注册操作。

    调用该方法后，本类库会自动的向服务器注册该账户。

    .. note:: 一般来说，开发者不需要直接使用该方法，而是通过 :cpp:func:`Lib::AccountList::Add` 将账户对象加入到列表，此时，本方法会被自动调用。

.. cpp:function:: void Account::Del()

    将账户数据从全局数据中删除。一旦调用，此账户将从服务器注销，且该账户的当前通话会断开。

    .. note:: 开发者不应直接使用该方法。本类的析构函数会自动完成这一步骤。

.. cpp:function:: void Account::AnswerCall()

    接听当前来电

.. cpp:function:: void Account::AnswerCall(CallInfo* callInfo)

    接听来电

.. cpp:function:: void Account::HangupCall()

    挂断当前通话；拒接当前来电

.. cpp:function:: void Account::HangupCall(CallInfo* callInfo)

    挂断通话；拒接来电

.. cpp:function:: void Account::MakeCall(String* callee)

    :param callee: 被叫

    发起呼叫

    被叫号码直接指定号码/用户名即可， **不要** 使用类似 ``sip:123@host`` 这样的格式。


.. cpp:function:: void Account::SendDtmf(String* digits)

    :param digits: DTFM 码字符串

    在当前通话中发送DTMF码

.. cpp:function:: void Account::SendDtmf(CallInfo* callInfo, String* digits)

    :param callInfo: 要发送DTMF码的通话
    :param digits: DTFM 码字符串

    发送DTMF码

.. cpp:function:: void Account::Register()

    注册

    调用后，当前账户将相服务器进行注册。

    .. note:: 由于调用 :cpp:func:`Account::Add` 或者 :cpp:func:`Lib::AccountList::Add` 都会自动注册账户，所以开发者一般不需要使用该方法。

.. cpp:function:: void Account::UnRegister()

    反注册

    调用后，本类库将向服务器发送该账户的反注册请求。

.. cpp:function:: void Account::StartRereg()

    该方法用于在注册请求返回 ``488`` 时，继续尝试注册。开发者请 **不要调用** 。

.. cpp:function:: void Account::StopRereg()

    该方法用于配合 :cpp:func:`Account::StartRereg` 使用。开发者请 **不要调用** 。

属性
****

.. cpp:member:: RegInfo* Account::LastRegInfo

    最近一次的注册结果信息

.. cpp:member:: CallInfo* Account::CurrentCallInfo

    当前正在进行的通话或者来电的信息

.. cpp:member:: int Account::Id

    账户ID

    注意这个ID不是全局唯一的

.. cpp:member:: String* Account::UserId

    账户的用户ID字符串

    格式是::

        "project_name" <sip:user_name@server_address>

.. cpp:member:: String* Account::RegUri

    注册URI

    格式是::

        sip:host[:port]

.. cpp:member:: String* Account::Realm

    领域

.. cpp:member:: String* Account::Project

    项目名称

.. cpp:member:: String* Account::UserName

    注册用户名

.. cpp:member:: String* Account::Password

    注册密码

.. cpp:member:: String* Account::Label

    账户标签

.. cpp:member:: Boolean* Account::ReRegOn488

    是否在收到 ``status=488`` 的注册回复后，仍然尝试重注册。

    :默认值: ``false``

.. cpp:member:: Object* Account::tag

    标签

事件
****

.. cpp:function:: void Account::OnRegisterStateChanged(RegInfo* regInfo)

    :param regInfo: 注册信息对象

    账户的注册状态发生变化

.. cpp:function:: void Account::OnIncomingCall(CallInfo* info)

    :param info: 呼叫信息对象

    收到来电

    参数 ``info`` 是一个 :cpp:class:`CallInfo` 对象。开发者可以在该事件的处理函数中：可以调用 :cpp:func:`CallInfo::Answer` 接听；可以调用 :cpp:func:`CallInfo::Hangup` 拒接。

    开发者也可以直接调用 :cpp:func:`Account::AnswerCall` 与 :cpp:func:`Account::HangupCall` 来进行接听与拒接。

.. cpp:function:: void Account::OnCallStateChanged(CallInfo* regInfo)

    :param info: 呼叫信息对象

    呼叫状态发生变化